package cn.xtits.xtp.service;
import cn.xtits.xtp.entity.Area;
import cn.xtits.xtp.entity.AreaExample;
import java.util.List;

/**
 * Created by Dan on 2018-02-28 01:53:21
 */
public interface AreaService {

    int deleteByPrimaryKey(Integer id);

    int insert(Area record);

    List<Area> listByExample(AreaExample example);

    Area getByPrimaryKey(Integer id);

    int updateByPrimaryKey(Area record);

    int updateByPrimaryKeySelective(Area record);

}