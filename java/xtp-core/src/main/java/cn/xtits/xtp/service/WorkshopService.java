package cn.xtits.xtp.service;
import cn.xtits.xtp.entity.Workshop;
import cn.xtits.xtp.entity.WorkshopExample;
import java.util.List;

/**
 * Created by Generator 2018-08-21 09:16:58
 */
public interface WorkshopService {

    int deleteByPrimaryKey(Integer id);

    int insert(Workshop record);

    List<Workshop> listByExample(WorkshopExample example);

    Workshop getByPrimaryKey(Integer id);

    int updateByPrimaryKey(Workshop record);

    int updateByPrimaryKeySelective(Workshop record);

}