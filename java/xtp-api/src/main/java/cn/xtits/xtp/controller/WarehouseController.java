package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.utils.JsonUtil;
import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.common.web.Pagination;
import cn.xtits.xtp.entity.Warehouse;
import cn.xtits.xtp.entity.WarehouseExample;
import cn.xtits.xtp.service.WarehouseService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @fileName: WareHouseController.java
 * @author: Dan
 * @createDate: 2018-02-28 13:50:27
 * @description: 仓库表
 */
@RestController
@RequestMapping("/warehouse")
public class WarehouseController extends BaseController {

    @Autowired
    private WarehouseService service;

    @RequiresPermissions("ibasic-warehouse:insert")
    @RequestMapping(value = "insertWarehouse")
    public AjaxResult insertWarehouse(
            @RequestParam(value = "data", required = false) String data) {
        Warehouse record = JsonUtil.fromJson(data, Warehouse.class);
        if (exist(record) > 0) {
            return new AjaxResult(-2, "存在相同的编码或名称!");
        }
        //Date dt = getDateNow();
        record.setStatus(1);
        record.setCreateDate(null);
        record.setMakeBillMan(getUserName());
        record.setModifier(getUserName());
        record.setModifyDate(null);
        record.setDeleteFlag(false);
        int row = service.insert(record);
        return new AjaxResult(row);
    }

    @RequiresPermissions("ibasic-warehouse:delete")
    @RequestMapping(value = "deleteWarehouse")
    public AjaxResult deleteWarehouse(
            @RequestParam(value = "id", required = false) int id) {
        Warehouse record = new Warehouse();
        record.setId(id);
        record.setDeleteFlag(true);
        record.setModifier(getUserName());
        record.setModifyDate(null);
        int row = service.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    @RequiresPermissions("ibasic-warehouse:update")
    @RequestMapping(value = "updateWarehouse")
    public AjaxResult updateWarehouse(
            @RequestParam(value = "data", required = false) String data) {
        Warehouse record = JsonUtil.fromJson(data, Warehouse.class);
        if (exist(record) > 0) {
            return new AjaxResult(-2, "存在相同的编码或名称!");
        }
        record.setCreateDate(null);
        record.setMakeBillMan(null);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(false);
        int row = service.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    @RequestMapping(value = "listWarehouse")
    public AjaxResult listWarehouse(
            @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "location", required = false) String location,
            @RequestParam(value = "manager", required = false) String manager,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        WarehouseExample example = new WarehouseExample();
        example.setPageIndex(pageIndex);
        example.setPageSize(pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            example.setOrderByClause(orderBy);
        }
        WarehouseExample.Criteria criteria = example.createCriteria();

        criteria.andDeleteFlagEqualTo(false);
        if (StringUtils.isNotBlank(code)) {
            criteria.andCodeLike(code);
        }
        if (StringUtils.isNotBlank(name)) {
            criteria.andNameLike(name);
        }
        if (StringUtils.isNotBlank(location)) {
            criteria.andLocationLike(location);
        }
        if (StringUtils.isNotBlank(manager)) {
            criteria.andManagerLike(manager);
        }
        if (StringUtils.isNotBlank(startDate)) {
            criteria.andCreateDateGreaterThanOrEqualTo(DateTime.parse(startDate, format).toDate());
        }
        if (StringUtils.isNotBlank(endDate)) {

            criteria.andCreateDateLessThanOrEqualTo(DateTime.parse(endDate, format).toDate());
        }

        List<Warehouse> list = service.listByExample(example);
        Pagination<Warehouse> pList = new Pagination<>(example,list,example.getCount());
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "getWarehouse")
    public AjaxResult getWarehouse(@RequestParam(value = "id", required = false) Integer id) {
        Warehouse res = service.getByPrimaryKey(id);
        return new AjaxResult(res);
    }

    private int exist(Warehouse record) {
        {
            WarehouseExample example = new WarehouseExample();
            WarehouseExample.Criteria criteria = example.createCriteria();
            criteria.andDeleteFlagEqualTo(false);
            if (record.getId() != null && record.getId() > 0) {
                criteria.andIdNotEqualTo(record.getId());
            }
            criteria.andCodeEqualTo(record.getCode());
            List<Warehouse> list = service.listByExample(example);
            if (list.size() > 0) {
                return list.size();
            }
        }
        {
            WarehouseExample example = new WarehouseExample();
            WarehouseExample.Criteria criteria = example.createCriteria();
            criteria.andDeleteFlagEqualTo(false);
            if (record.getId() != null && record.getId() > 0) {
                criteria.andIdNotEqualTo(record.getId());
            }
            criteria.andNameEqualTo(record.getName());
            List<Warehouse> list = service.listByExample(example);
            if (list.size() > 0) {
                return list.size();
            }
        }
        return 0;
    }
}