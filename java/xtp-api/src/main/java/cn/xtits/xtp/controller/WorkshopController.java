package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.utils.JsonUtil;
import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.common.web.Pagination;
import cn.xtits.xtp.entity.Workshop;
import cn.xtits.xtp.entity.WorkshopExample;
import cn.xtits.xtp.service.WorkshopService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
* @fileName: WorkshopController.java
* @author: Dan
* @createDate: 2018-08-21 09:16:58
* @description: 车间信息
*/
@RestController
@RequestMapping("/workshop")
public class WorkshopController extends BaseController {

    @Autowired
    private WorkshopService workshopService;

    //@RequiresPermissions("workshop:insert")
    @RequestMapping(value = "insertWorkshop")
        public AjaxResult insertWorkshop(
                @RequestParam(value = "data", required = false) String data) {
        Workshop record = JsonUtil.fromJson(data, Workshop.class);
        //Date dt = getDateNow();
        record.setCreateDate(null);
        record.setMakeBillMan(getUserName());
        record.setModifier(getUserName());
        record.setModifyDate(null);
        record.setDeleteFlag(false);
        workshopService.insert(record);
        return new AjaxResult(record);
    }

    //@RequiresPermissions("workshop:delete")
    @RequestMapping(value = "deleteWorkshop")
    public AjaxResult deleteWorkshop(
            @RequestParam(value = "id", required = false) int id) {
        Workshop record = new Workshop();
        record.setId(id);
        record.setDeleteFlag(true);
        record.setModifier(getUserName());
        record.setModifyDate(null);
        int row = workshopService.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    //@RequiresPermissions("workshop:update")
    @RequestMapping(value = "updateWorkshop")
    public AjaxResult updateWorkshop(
            @RequestParam(value = "data", required = false) String data) {
        Workshop record = JsonUtil.fromJson(data, Workshop.class);
        record.setCreateDate(null);
        record.setMakeBillMan(null);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(false);
        workshopService.updateByPrimaryKeySelective(record);
        return new AjaxResult(record);
    }

    @RequestMapping(value = "listWorkshop")
    public AjaxResult listWorkshop(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        WorkshopExample example = new WorkshopExample();
        example.setPageIndex(pageIndex);
        example.setPageSize(pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            example.setOrderByClause(orderBy);
        }
        WorkshopExample.Criteria criteria = example.createCriteria();

        criteria.andDeleteFlagEqualTo(false);
        if (StringUtils.isNotBlank(name)) {
            criteria.andNameLike(name);
        }
        if (StringUtils.isNotBlank(code)) {
            criteria.andCodeLike(code);
        }
        if (StringUtils.isNotBlank(startDate)) {
            criteria.andCreateDateGreaterThanOrEqualTo(DateTime.parse(startDate, format).toDate());
        }
        if (StringUtils.isNotBlank(endDate)) {

            criteria.andCreateDateLessThanOrEqualTo(DateTime.parse(endDate, format).toDate());
        }

        List<Workshop> list = workshopService.listByExample(example);
        Pagination<Workshop> pList = new Pagination<>(example,list,example.getCount());
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "getWorkshop")
    public AjaxResult getWorkshop(
            @RequestParam(value = "id", required = false) Integer id) {
        Workshop res = workshopService.getByPrimaryKey(id);
        return new AjaxResult(res);
    }


}