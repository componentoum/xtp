package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.utils.JsonUtil;
import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.common.web.Pagination;
import cn.xtits.xtp.entity.FormConfig;
import cn.xtits.xtp.entity.FormConfigExample;
import cn.xtits.xtp.service.FormConfigService;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @fileName: FormConfigController.java
 * @author: Dan
 * @createDate: 2018-05-11 10:31:52
 * @description: 表单配置
 */
@RestController
@RequestMapping("/formConfig")
public class FormConfigController extends BaseController {

    @Autowired
    private FormConfigService formConfigService;

    @RequestMapping(value = "listFormKeyByFormConfig")
    public AjaxResult listFormKeyByFormConfig(
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageIndex", required = false, defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        Map<String, Object> map = new HashMap<>();
        map.put("pageIndex", (pageIndex - 1) * pageSize);
        map.put("pageSize", pageIndex * pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            map.put("orderBy", orderBy);
        }
        Integer totalCount = formConfigService.listFormKeyByFormConfigCount(map);
        List<FormConfig> formConfigList = null;
        if (totalCount != null && totalCount > 0) {
            formConfigList = formConfigService.listFormKeyByFormConfig(map);
        }
        Pagination<FormConfig> pList = new Pagination<>(pageSize, pageIndex, formConfigList == null ? new ArrayList<>() : formConfigList, totalCount);
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "insertFormConfig")
    public AjaxResult insertFormConfig(
            @RequestParam(value = "data", required = false) String data) {
        FormConfig record = JsonUtil.fromJson(data, FormConfig.class);
        //Date dt = getDateNow();
        if (exist(record) > 0) {
            return new AjaxResult(-2, "存在相同的表名称和字段名称!");
        }
        record.setCreateDate(null);
        record.setMakeBillMan(getUserName());
        record.setModifier(getUserName());
        record.setModifyDate(null);
        record.setDeleteFlag(false);
        formConfigService.insert(record);
        return new AjaxResult(record);
    }

    @RequestMapping(value = "deleteFormConfig")
    public AjaxResult deleteFormConfig(
            @RequestParam(value = "id", required = false) int id) {
        FormConfig record = new FormConfig();
        record.setId(id);
        record.setDeleteFlag(true);
        record.setModifier(getUserName());
        record.setModifyDate(null);
        int row = formConfigService.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    @RequestMapping(value = "updateFormConfig")
    public AjaxResult updateFormConfig(
            @RequestParam(value = "data", required = false) String data) {
        FormConfig record = JsonUtil.fromJson(data, FormConfig.class);
        if (exist(record) > 0) {
            return new AjaxResult(-2, "存在相同的表名称和字段名称!");
        }
        record.setCreateDate(null);
        record.setMakeBillMan(null);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(false);
        formConfigService.updateByPrimaryKeySelective(record);
        return new AjaxResult(record);
    }

    @RequestMapping(value = "listFormConfig")
    public AjaxResult listFormConfig(
            @RequestParam(value = "formKey", required = false) String formKey,
            @RequestParam(value = "fieldCode", required = false) String fieldCode,
            @RequestParam(value = "listSort", required = false) String listSort,
            @RequestParam(value = "listShowFlag", required = false) Boolean listShowFlag,
            @RequestParam(value = "editSort", required = false) String editSort,
            @RequestParam(value = "editShowFlag", required = false) Boolean editShowFlag,
            @RequestParam(value = "editFlag", required = false) Boolean editFlag,
            @RequestParam(value = "requiredFlag", required = false) Boolean requiredFlag,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        FormConfigExample example = new FormConfigExample();
        example.setPageIndex(pageIndex);
        example.setPageSize(pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            example.setOrderByClause(orderBy);
        }
        FormConfigExample.Criteria criteria = example.createCriteria();

        criteria.andDeleteFlagEqualTo(false);
        if (StringUtils.isNotBlank(formKey)) {
            criteria.andFormKeyLike(formKey);
        }
        if (StringUtils.isNotBlank(fieldCode)) {
            criteria.andFieldCodeLike(fieldCode);
        }
        if (StringUtils.isNotBlank(listSort)) {
            String[] split = listSort.split(",");
            if (split.length > 1) {
                List<Integer> listSortList = new ArrayList<>();
                for (String s : split) {
                    listSortList.add(Integer.parseInt(s));
                }
                criteria.andListSortIn(listSortList);
            } else {
                criteria.andListSortEqualTo(Integer.parseInt(split[0]));
            }
        }

        if (listShowFlag != null) {
            criteria.andListShowFlagEqualTo(listShowFlag);
        }
        if (StringUtils.isNotBlank(editSort)) {
            String[] split = editSort.split(",");
            if (split.length > 1) {
                List<Integer> editSortList = new ArrayList<>();
                for (String s : split) {
                    editSortList.add(Integer.parseInt(s));
                }
                criteria.andEditSortIn(editSortList);
            } else {
                criteria.andEditSortEqualTo(Integer.parseInt(split[0]));
            }
        }

        if (editShowFlag != null) {
            criteria.andEditShowFlagEqualTo(editShowFlag);
        }
        if (editFlag != null) {
            criteria.andEditFlagEqualTo(editFlag);
        }
        if (requiredFlag != null) {
            criteria.andRequiredFlagEqualTo(requiredFlag);
        }
        if (StringUtils.isNotBlank(startDate)) {
            criteria.andCreateDateGreaterThanOrEqualTo(DateTime.parse(startDate, format).toDate());
        }
        if (StringUtils.isNotBlank(endDate)) {

            criteria.andCreateDateLessThanOrEqualTo(DateTime.parse(endDate, format).toDate());
        }

        List<FormConfig> list = formConfigService.listByExample(example);
        Pagination<FormConfig> pList = new Pagination<>(example, list, example.getCount());
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "getFormConfig")
    public AjaxResult getFormConfig(
            @RequestParam(value = "id", required = false) Integer id) {
        FormConfig res = formConfigService.getByPrimaryKey(id);
        return new AjaxResult(res);
    }

    private int exist(FormConfig record) {

        FormConfigExample example = new FormConfigExample();
        FormConfigExample.Criteria criteria = example.createCriteria();
        criteria.andDeleteFlagEqualTo(false);
        if (record.getId() != null && record.getId() > 0) {
            criteria.andIdNotEqualTo(record.getId());
        }
        criteria.andFieldCodeEqualTo(record.getFieldCode());
        criteria.andFormKeyEqualTo(record.getFormKey());

        return formConfigService.listByExample(example).size();

    }

}