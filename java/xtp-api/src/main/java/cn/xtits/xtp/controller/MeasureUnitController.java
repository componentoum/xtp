package cn.xtits.xtp.controller;

import cn.xtits.xtf.common.utils.JsonUtil;
import cn.xtits.xtf.common.web.AjaxResult;
import cn.xtits.xtf.common.web.Pagination;
import cn.xtits.xtp.entity.MeasureUnit;
import cn.xtits.xtp.entity.MeasureUnitExample;
import cn.xtits.xtp.service.MeasureUnitService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @fileName: MeasureUnitController.java
 * @author: Dan
 * @createDate: 2018-02-28 13:43:14
 * @description: MeasureUnit
 */
@RestController
@RequestMapping("/measureUnit")
public class MeasureUnitController extends BaseController {

    @Autowired
    private MeasureUnitService measureUnitService;

    @RequiresPermissions("ibasic-measure-unit:insert")
    @RequestMapping(value = "insertMeasureUnit")
    public AjaxResult insertMeasureUnit(
            @RequestParam(value = "data", required = false) String data) {
        MeasureUnit record = JsonUtil.fromJson(data, MeasureUnit.class);
        if (exist(record) > 0) {
            return new AjaxResult(-2, "存在相同的单位名称!");
        }
        //Date dt = getDateNow();
        record.setCreateDate(null);
        record.setMakeBillMan(getUserName());
        record.setModifier(getUserName());
        record.setModifyDate(null);
        record.setDeleteFlag(false);
        measureUnitService.insert(record);
        return new AjaxResult(record);
    }

    @RequiresPermissions("ibasic-measure-unit:delete")
    @RequestMapping(value = "deleteMeasureUnit")
    public AjaxResult deleteMeasureUnit(
            @RequestParam(value = "id", required = false) int id) {
        MeasureUnit record = new MeasureUnit();
        record.setId(id);
        record.setDeleteFlag(true);
        record.setModifier(getUserName());
        record.setModifyDate(null);
        int row = measureUnitService.updateByPrimaryKeySelective(record);
        return new AjaxResult(row);
    }

    @RequiresPermissions("ibasic-measure-unit:update")
    @RequestMapping(value = "updateMeasureUnit")
    public AjaxResult updateMeasureUnit(
            @RequestParam(value = "data", required = false) String data) {
        MeasureUnit record = JsonUtil.fromJson(data, MeasureUnit.class);
        if (exist(record) > 0) {
            return new AjaxResult(-2, "存在相同的单位名称!");
        }
        record.setCreateDate(null);
        record.setMakeBillMan(null);
        record.setModifyDate(null);
        record.setModifier(getUserName());
        record.setDeleteFlag(false);
        measureUnitService.updateByPrimaryKeySelective(record);
        return new AjaxResult(record);
    }

    @RequestMapping(value = "listMeasureUnit")
    public AjaxResult listMeasureUnit(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate) {
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        MeasureUnitExample example = new MeasureUnitExample();
        example.setPageIndex(pageIndex);
        example.setPageSize(pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            example.setOrderByClause(orderBy);
        }
        MeasureUnitExample.Criteria criteria = example.createCriteria();

        criteria.andDeleteFlagEqualTo(false);
        if (StringUtils.isNotBlank(name)) {
            criteria.andNameLike(name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            criteria.andCreateDateGreaterThanOrEqualTo(DateTime.parse(startDate, format).toDate());
        }
        if (StringUtils.isNotBlank(endDate)) {

            criteria.andCreateDateLessThanOrEqualTo(DateTime.parse(endDate, format).toDate());
        }

        List<MeasureUnit> list = measureUnitService.listByExample(example);
        Pagination<MeasureUnit> pList = new Pagination<>(example, list, example.getCount());
        return new AjaxResult(pList);
    }

    @RequestMapping(value = "getMeasureUnit")
    public AjaxResult getMeasureUnit(@RequestParam(value = "id", required = false) Integer id) {
        MeasureUnit res = measureUnitService.getByPrimaryKey(id);
        return new AjaxResult(res);
    }

    /**
     * 是否存在相同数据
     *
     * @param entity
     * @return 返回相同数据的条数
     */
    private int exist(MeasureUnit entity) {
        MeasureUnitExample example = new MeasureUnitExample();
        MeasureUnitExample.Criteria criteria = example.createCriteria();
        criteria.andDeleteFlagEqualTo(false);
        if (null != entity.getId() && entity.getId() > 0) {
            criteria.andIdNotEqualTo(entity.getId());
        }
        criteria.andNameEqualTo(entity.getName());
        List<MeasureUnit> list = measureUnitService.listByExample(example);
        return list.size();
    }


}