package cn.xtits.xtp.enums;

/**
 * @fileName: CodingRuleEnum
 * @author: Dan
 * @createDate: 2018-06-08 10:17.
 * @description: 编码规则枚举
 */
public enum CodingRuleTypeEnum {

    IDENTITY_FIELD(1, "自增字段", "identity"),

    DATE_FIELD(2, "时间字段", "date"),

    FIXED_FIELD(3, "固定字段", "fixed");

    CodingRuleTypeEnum(int type, String name, String key) {
        this.type = type;
        this.name = name;
        this.key = key;
    }

    public int type;

    public String name;

    public String key;

}
