import { IBASIC } from "../../api";
import { Message } from "element-ui";

const Color = function() {
  return {
    id: 0,
    code: null,
    name: null
  };
},
ColorAPI = {
  color: new Color(),
  get: () => ColorAPI.color,
  set: (value) => {
    ColorAPI.color = Object.assign(ColorAPI.color, value);
  },
  init: () => new Color(),
  getColor(params) {
    return new Promise((resolve) => {
      IBASIC.color.getColor(params).then(({data, res}) => {
        let item = {};
        if (data.code === 1) {item = data.data;}
        resolve({ data, item, res });
        });
    });
  },
  listColor(params) {
    return new Promise((resolve) => {
      IBASIC.color.listColor(params).then(({data, res}) => {
        let list = [];
        if (data.code === 1) {list = data.data.data;}
        resolve({ data, list, res });
        });
    });
  },
  insertColor(params) {
    return new Promise((resolve) => {
      IBASIC.color.insertColor(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "新增成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  updateColor(params) {
    return new Promise((resolve) => {
      IBASIC.color.updateColor(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "修改成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  },
  deleteColor(params) {
    return new Promise((resolve) => {
      IBASIC.color.deleteColor(params).then(({data, res}) => {
        if (data.code === 1) {
          Message({
            message: "删除成功",
            type: "success"
          });
        }
        resolve({data, res});
        });
    });
  }
};

export { ColorAPI };
