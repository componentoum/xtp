import {_axios} from "./axios.config";
import {GetUrl, HttpRequest} from "./http-request";

const ibasicUrl = GetUrl("IBASIC").url,
  IBASIC = HttpRequest(ibasicUrl, [
    "area",
    "codingRule",
    "color",
    "custom",
    "farmer",
    "dict",
    "formConfig",
    "measureUnit",
    "supplier",
    "warehouse"
  ]).api;
IBASIC.area.listAreaTree = (params) => _axios.post(ibasicUrl + "/area/listAreaTree", {...params});
IBASIC.formConfig.listFormKeyByFormConfig = (params) => _axios.post(ibasicUrl + "/formConfig/listFormKeyByFormConfig", {...params});
export {
  IBASIC
};
